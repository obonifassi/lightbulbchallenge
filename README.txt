LightBulbChallenge

There are N light bulbs lined up in a row in a long room. 
Each bulb has its own switch and is currently switched off. 
The room has an entry door and an exit door.
There are P people lined up outside the entry door. 
Each bulb is numbered consecutively from 1 to N. So is each person. Person No. 1 enters the room, switches on every bulb, and exits. Person No. 2 enters and flips the switch on every second bulb (turning off bulbs 2, 4, 6, ...). Person No. 3 enters and flips the switch on every third bulb (changing the state on bulbs 3, 6, 9, ...). 
This continues until all P people have passed through the room. 
This project determines which light bulbs are illuminated after the Nth person has passed through the room.

Getting Started

To view/run, simply do the following:

1) Download solution to local machine
2) Extract files to a folder on local machine
3) Open index.html on a browser, input parameters for N lightbulbs and P people
4) Run