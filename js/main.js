 var app = angular.module('app', []);
    
    app.controller('ctrl', function ($scope) {
        $scope.lightbulbs = 0;
        $scope.people = 0;
        $scope.arr = [];
        $scope.r = 0;
                
        $scope.generateData = function () {
            
            var map = {};
            var people = parseInt($scope.people);
            var lightbulbs = parseInt($scope.lightbulbs);

            var limit = people * lightbulbs;

            if (limit < 30000) {

                for (var i = 1; i <= people; i++) {

                    if (i > lightbulbs) {
                        break;
                    }
                    else {
                        //var a = performance.now();
                        for (var k = i; k <= lightbulbs; (k = k + i)) {

                            //if (k % i == 0) {
                                if (map[k]) {
                                    if (map[k] == 0) {
                                        map[k] = 1;
                                    }
                                    else {
                                        map[k] = 0;
                                    }
                                }
                                else {
                                    map[k] = 1;
                                }
                            ///}
                        }
                        //var b = performance.now();
                        //var result = b - a;
                        //console.log("result: " + result);
                    }
                }
            }
            else {
                $scope.r = 1;
            }
            $scope.arr = map;
        }
    });